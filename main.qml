import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick3D 1.15
import QtQuick3D.Materials 1.15
import QtQuick3D.Helpers 1.15

import "House"

Window {
  id: root

  // cube rotation
  property real rotationX : 0.0
  property real rotationY : 0.7
  property real rotationZ : 0.0

  // "sun" position and brightness
  property real sunX : 0.3
  property real sunY : 0.4
  property real sunZ : 0.0
  property real sunBrightness: 0.5

  // turn on/off default light
  property alias isLightOn: lightSwitch.checked

  // turn on/off sun
  property alias isSunOn: sunSwitch.checked

  width: 1200
  height: 900

  visible: true
  title: qsTr("Qt 3D")

  View3D {
    id: view

    anchors {
      top: parent.top
      bottom: parent.bottom
      right: parent.right
      left: sideMenuDragger.right
    }

    environment: SceneEnvironment {
      clearColor: "#112220"
      backgroundMode: SceneEnvironment.Color
    }

    Node {
      id: scene

      PerspectiveCamera {
        id: camera

        z: 200
      }

      DirectionalLight {
        id: directionalLight

        brightness: 700

        z: 1000
        visible: root.isLightOn
      }

      Model {
        id: sun

        position: Qt.vector3d(root.sunX * 100, root.sunY * 100, 100 + root.sunZ * 100)
        scale: Qt.vector3d(0.2, 0.2, 0.2)
        source: "#Sphere"

        materials: [ DefaultMaterial {
            diffuseColor: "yellow"
          }
        ]

        visible: root.isSunOn

        PointLight {
          id: sunLight

          color: "yellow"
          ambientColor: "yellow"
          brightness: root.sunBrightness * 3000
        }
      }

      House {
        id: house

        eulerRotation: Qt.vector3d(root.rotationX * 360, root.rotationY * 360, root.rotationZ * 360)

        scale: {"50, 50, 50"}
      }
    }
  }

  WasdController {
    id: controller

    controlledObject: camera
    focus: true
  }

  Drawer {
    id: sideMenuDrawer

    width: parent.width * 0.25
    height: parent.height

    dim: false
    visible: true

    background: Rectangle {
      color: "#30a885"
    }

    ListView {
      id: slidersList

      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
        margins: 10
      }

      height: contentHeight

      model: ["rotationX", "rotationY", "rotationZ", "sunX", "sunY", "sunZ", "sunBrightness"]

      spacing: 20

      delegate: Item {

        width: parent.width
        height: 60

        Text {
          id: sliderText

          anchors {
            top: parent.top
            left: parent.left
          }

          width: parent.width
          height: parent.height / 2

          text: modelData + " : " + root[modelData].toFixed(2)

          font.pointSize: 16
        }

        Slider {
          id: slider

          from: 0
          to: 1
          value: root[modelData]

          anchors {
            bottom: parent.bottom
            left: parent.left
          }

          width: parent.width
          height: parent.height / 2

          onValueChanged: {
            console.log("Value at index: " + index + " -> "+ root[modelData].toFixed(2))

            root[modelData] = value
          }
        }
      }
    }

    Rectangle {
      id: lightingOptionsRec

      anchors {
        left: parent.left
        right: parent.right
        top: slidersList.bottom
        margins: 10
      }

      color: "#33b875"
      width: parent.width
      height: 100

      CheckBox {
        id: lightSwitch

        anchors {
          left: parent.left
          right: parent.right
          top: parent.top
        }

        height: parent.height / 2

        checked: true

        text: "Default light ON/OFF"

        font.pointSize: 16
      }

      CheckBox {
        id: sunSwitch

        anchors {
          left: parent.left
          right: parent.right
          top: lightSwitch.bottom
        }

        height: parent.height / 2

        checked: true

        text: "Sun ON/OFF"

        font.pointSize: 16
      }
    }

    onClosed: {
      controller.focus = true
    }
  }

  Rectangle {
    id: sideMenuDragger

    x: sideMenuDrawer.visible? sideMenuDrawer.x + sideMenuDrawer.width : 0

    width: 50
    height: parent.height
    color: "#218165"

    Text {
      id: openCloseText

      anchors.centerIn: parent

      rotation: sideMenuDrawer.visible ? -180 : 0

      text: ">"

      font.pointSize: 32
      color: "white"

      Behavior on rotation {
        PropertyAnimation {
          duration: 200
        }
      }

    }

    MouseArea {
      anchors.fill: parent

      onClicked:  {
        sideMenuDrawer.visible = !sideMenuDrawer.visible
      }
    }
  }

  Text {
    id: controllingText

    anchors {
      bottom: parent.bottom
      right: parent.right
    }

    text: "Control using W, A, S, D, R, F keys and mouse"

    font.pointSize: 20
    color: "white"
  }
}
