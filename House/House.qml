import QtQuick 2.15
import QtQuick3D 1.15

Node {
    id: rOOT

    Model {
        id: cube
        y: -0.404242
        source: "meshes/cube.mesh"

        PrincipledMaterial {
            id: material_material
            baseColor: "#ff0f0300"
            metalness: 0
            roughness: 0.5
            cullMode: Material.NoCulling
        }

        PrincipledMaterial {
            id: material_001_material
            baseColor: "#ff013803"
            metalness: 0
            roughness: 0.5
            cullMode: Material.NoCulling
        }

        PrincipledMaterial {
            id: material_002_material
            baseColor: "#4500ccbb"
            metalness: 0.485294
            roughness: 0.5
            emissiveColor: "#ff0b1335"
            cullMode: Material.NoCulling
        }
        materials: [
            material_material,
            material_001_material,
            material_002_material
        ]
    }


}
